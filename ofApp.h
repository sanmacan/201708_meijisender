#pragma once

#include "ofMain.h"
#include "ofxUI.h"
#include "ofxOsc.h"
#include "ofxKinect.h"
#include "ofxOpenCv.h"

//#define HOST "169.254.60.219"
//#define HOST "10.190.237.193"
#define HOST "localhost"
#define PORT 2513

class ofApp : public ofBaseApp
{
public:
    void setup();
    void update();
    void draw();
    void exit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int _w, int _h);
    
    //----------------------------------------------- basic
    float w, h;
    float halfWidth, halfHeight;
    int frameRate;
    float threshold;
    float scale;
    
    //----------------------------------------------- kinect
    ofxKinect kinect;
    float kw, kh;
    float near, far;
    float minArea, maxArea;
    float kinectScale;
    float ms;//miniScale
    int kinectAngle;
    
    //----------------------------------------------- openCv
    ofxCvColorImage colorImage;
    ofxCvGrayscaleImage grayImage;
    ofxCvGrayscaleImage depthImage;
    ofxCvGrayscaleImage diffImage;
    ofxCvContourFinder finder;
    
    ofPixels getBinarizedPixels( float _near, float _far );
    ofPixels getDifferencePixels();
    
    //----------------------------------------------- gui
    ofxUICanvas* gui;
    
    void setupGUI();
    void guiEvent( ofxUIEventArgs &e );
    
    //----------------------------------------------- osc
    ofxOscSender sender;
    void sendOF( ofVec2f _centroid );
};
