#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    //----------------------------------------------- basic
    w = ofGetWidth();
    h = ofGetHeight();
    
    halfWidth   = w / 2;
    halfHeight = h / 2;
    
    frameRate = 60;
    
    scale = 1.7;
    
    //----------------------------------------------- osc
    sender.setup( HOST, PORT );
    
    //----------------------------------------------- kinect
    kinect.listDevices();
    kinect.init();
    kinect.open();
    kinect.setRegistration(true);
    kinect.setLed( ( ofxKinect::LedMode )LED_OFF );
    
    kw = kinect.getWidth();
    kh = kinect.getHeight();
    
    kinectScale = kw * kh;
    ms = 0.5;
    
    kinectAngle = 0;
    kinect.setCameraTiltAngle( kinectAngle );
    
    //----------------------------------------------- openCv
    colorImage.allocate( kw, kh );
    grayImage.allocate( kw, kh );
    depthImage.allocate( kw, kh );
    diffImage.allocate( kw, kh );
    
    threshold = 50;
    
    minArea = 5;
    maxArea = 1000;
    
    //----------------------------------------------- gui
    setupGUI();
    
    //----------------------------------------------- app
    ofSetFrameRate( frameRate );
    ofSetVerticalSync( true );
    ofDisableAntiAliasing();
    ofShowCursor();
    
    ofBackground( 255 );
}

//--------------------------------------------------------------
void ofApp::setupGUI()
{
    float xInit = OFX_UI_GLOBAL_WIDGET_SPACING;
    float length = 255 - xInit;
    
    int dim = 16;
    
    gui = new ofxUICanvas( 10, 10, length, h );
    
    //------------------------------------------------------------ title
    gui->addLabel( "BreakTime_Sender", OFX_UI_FONT_LARGE );
    
    //------------------------------------------------------------ fps
    gui->addSpacer( length - xInit, 2 );
    gui->addWidgetDown( new ofxUILabel( "FPS", OFX_UI_FONT_MEDIUM ) );
    
    gui->addFPSSlider( "FPS", length - xInit, dim );
    
    gui->addWidgetDown( new ofxUISlider( "Threshold", 0, 255, &threshold, (length - xInit ), dim ) );
    gui->addWidgetDown( new ofxUIRangeSlider( "Area", 5.0, 10000.0, &minArea, &maxArea, ( length - xInit ), dim ));
    gui->addWidgetDown( new ofxUIRangeSlider( "Depth", 50.0, 5000.0, &near, &far, ( length - xInit ), dim ));
    
    gui->autoSizeToFitWidgets();
    
    gui->setDrawWidgetPadding( true );
    
    ofAddListener( gui->newGUIEvent, this, &ofApp::guiEvent );
    
    gui->loadSettings( "GUI/guiSettings.xml" );
}

//--------------------------------------------------------------
void ofApp::update()
{
    
    kinect.update();
    
    if( kinect.isFrameNew() )
    {
        colorImage.setFromPixels( kinect.getPixels() );
        
        grayImage = colorImage;
        
        grayImage.threshold( threshold );
        
        depthImage.setFromPixels( getBinarizedPixels( near, far ) );
        
        diffImage.setFromPixels( getDifferencePixels() );
        diffImage.mirror( false, true );
        
        finder.findContours( diffImage, minArea, maxArea, 10, false );
        
        for( int i = 0; i < finder.blobs.size(); i++ )
            {
                sendOF( finder.blobs[ i ].centroid );
            }
        }
}

//--------------------------------------------------------------
void ofApp::sendOF( ofVec2f _centroid )
{
    ofxOscMessage m;
    m.setAddress( "/user/position" );
    m.addFloatArg( _centroid.x );
    m.addFloatArg( _centroid.y );
    
    sender.sendMessage( m );
}

//--------------------------------------------------------------
ofPixels ofApp::getBinarizedPixels( float _near, float _far )
{
    ofFloatPixels distPixels = kinect.getDistancePixels();
    ofPixels pixels;
    pixels.allocate( kw, kh, 1 );
    int nPixels = kw * kh;
    
    for( int i = 0; i < nPixels; i++ )
    {
        (( distPixels[ i ] > _near ) && ( distPixels[ i ] < _far )) ? pixels[ i ] = 255 : pixels[ i ] = 0;
    }
    return pixels;
}

//--------------------------------------------------------------
ofPixels ofApp::getDifferencePixels()
{
    ofPixels pixels;
    pixels.allocate( kw, kh, 1 );
    
    ofPixels currentPixels = grayImage.getPixels();
    ofPixels lastPixels    = depthImage.getPixels();
    
    for( int i = 0; i < ( kw * kh ); i++ )
    {
        ( ( currentPixels[ i ] == 255 ) && ( lastPixels[ i ] == 255 ) ) ? pixels.setColor( i, 255 ) : pixels.setColor( i, 0 );
    }
    
    return pixels;
}


//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetColor( 255 );

    grayImage.draw( kw, 0, kw * ms, kh * ms );
    depthImage.draw( kw, kh * ms, kw * ms, kh * ms );
    diffImage.draw( 0, 0 );
    
    for( int i = 0; i < finder.nBlobs; i++ )
    {
        finder.blobs[ i ].draw( 0, 0 );
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    //---------------------------------------- gui
    if( key == ' ' )
    {
        gui->toggleVisible();
    }
    
    
    //---------------------------------------- fullscreen
    if( key == 'f' )
    {
        ofToggleFullscreen();
    }
    
    //---------------------------------------- kinect
    switch (key)
    {
        case OF_KEY_UP:
            kinectAngle++;
            if( kinectAngle > 30 ) kinectAngle = 30;
            kinect.setCameraTiltAngle( kinectAngle );
            break;
            
        case OF_KEY_DOWN:
            kinectAngle--;
            if( kinectAngle < -30 ) kinectAngle = -30;
            kinect.setCameraTiltAngle( kinectAngle );
            break;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y )
{
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y)
{
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y)
{
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int _w, int _h)
{
    w = _w;
    h = _h;
    
    halfWidth  = w / 2.0f;
    halfHeight = h / 2.0f;
}

//--------------------------------------------------------------
void ofApp::exit()
{
    gui->saveSettings( "GUI/guiSettings.xml" );
    
    delete gui;
}

//--------------------------------------------------------------
void ofApp::guiEvent( ofxUIEventArgs &e )
{
    
}
